package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Reports;

public class SeleniumBase extends Reports implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i;

	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			//reportStep("The Element "+ele+" clicked", "pass"); 
			System.out.println("The Element "+ele+" clicked");

			//System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			//reportStep("The Element "+ele+" clicked", "fail"); 
			System.out.println("The Element "+ele+" clicked");

			//System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		}
	}
	
	
	public void clickWithNoSnap(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			System.out.println("The Element "+ele+" clicked");
		} catch (StaleElementReferenceException e) {
			System.err.println("The Element "+ele+" could not be clicked");
			throw new RuntimeException();
		}
	}
	
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			System.out.println("The field is cleared Successfully");
		} catch (ElementNotInteractableException e) {
			System.err.println("The field is not Interactable");
			throw new RuntimeException();

		}
	}

	
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			System.out.println("The Data :"+data+" entered Successfully");
		} catch (ElementNotInteractableException e) {
			System.err.println("The Element "+ele+" is not Interactable");
			throw new RuntimeException();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		String s=ele.getText();
		return s;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		String clrvalue=ele.getCssValue("color");
		return clrvalue;
	}

	@Override
	public String getTypedText(WebElement ele) {
		String attributeValue=ele.getAttribute("value");
		return attributeValue;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dropDown;
		try {
			dropDown = new Select(ele);
			dropDown.selectByVisibleText(value);
			System.out.println("The Element "+value+" selected");
		} 
		catch (Exception e) {
					}
		}

	public void selectDropDownUsingIndex(WebElement ele, int index) {
		
		try {
			Select dropDown = new Select(ele);
			dropDown.selectByIndex(index);
			System.out.println("The Element "+index+" selected");

		} catch (NoSuchElementException e) {
			// TODO Auto-generated catch block
		System.err.println("The Element "+index+" not selected");
		}
	}
     public void selectDropDownUsingValue(WebElement ele, String value) {
     try {
		Select dropDown=new Select(ele);
dropDown.selectByValue(value);
System.out.println("The Element "+value+" selected");
	} catch (Exception e) {
		e.printStackTrace();
	}

}
public boolean verifyExactText(WebElement ele, String expectedText) {
	if(ele.getText().equals(expectedText)) {
		System.out.println("The expected text contains the actual "+expectedText);
		return true;}
	else 
		{System.out.println("The expected text doesn't contain the actual "+expectedText);
	    return false;
}
	}

	
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		if(ele.getText().contains(expectedText)) {
			System.out.println("The expected text contains the actual "+expectedText);
			return true;}
		else 
			{System.out.println("The expected text doesn't contain the actual "+expectedText);
		    return false;
	}}
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(attribute).equals(value)) {
			System.out.println("The expected attribute :"+attribute+" value contains the actual "+value);
			return true;
		}else {
			System.out.println("The expected attribute :"+attribute+" value does not contains the actual "+value);
			 return false;
		}
}
			
public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
	if(ele.getAttribute(attribute).contains(value)) {
		System.out.println("The expected attribute :"+attribute+" value contains the actual "+value);
		
	}
	else {
		System.out.println("The expected attribute :"+attribute+" value does not contains the actual "+value);
		
	}}

public boolean verifyDisplayed(WebElement ele) {
	if(ele.isDisplayed()) {
		System.out.println("The element "+ele+" is visible");
	return true;
	} 
	else {
		System.out.println("The element "+ele+" is not visible");
	}
	return false;
}

	
	public boolean verifyDisappeared(WebElement ele) {
		
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			if(ele.isEnabled()) {
				System.out.println("The element "+ele+" is Enabled");
				return true;
			} else {
				System.out.println("The element "+ele+" is not Enabled");
			}}
		 catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}
		return false;
}
	public boolean verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				System.out.println("The element "+ele+" is selected");
				return true;
			} else {
				System.out.println("The element "+ele+" is not selected");
			}
		} catch (WebDriverException e) {
			System.out.println("WebDriverException : "+e.getMessage());
		}
		return false;
}
public void startApp(String url) {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		WebElement findElementById = driver.findElementById(value);
		return findElementById;
	}

	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type.toLowerCase()) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "link": return driver.findElementsByLinkText(value);
			case "xpath": return driver.findElementsByXPath(value);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+type+" Not Found with value: "+value);
			throw new RuntimeException();
		}
		return null;

	}

	public void switchToAlert() {
try {
	driver.switchTo().alert();
	System.out.println("switch to alert");
} catch (Exception e) {
}
finally{
	takeSnap();
}
	}

	
	public void acceptAlert() {
		String text = "";
try {
	Alert alert=driver.switchTo().alert();
	text=alert.getText();
	alert.accept();
	System.out.println("The alert "+text+" is accepted.");
}  catch (NoAlertPresentException e) {
	System.out.println("There is no alert present.");
} 
catch (WebDriverException e) {
	System.out.println("WebDriverException : "+e.getMessage());
}  
}
	
public void dismissAlert() {
	String text = "";
	try {
		Alert alert=driver.switchTo().alert();
		text=alert.getText();
		alert.dismiss();
		System.out.println("The alert "+text+" is accepted.");
	}  catch (NoAlertPresentException e) {
		System.out.println("There is no alert present.");
	} 
	catch (WebDriverException e) {
		System.out.println("WebDriverException : "+e.getMessage());
	}  
	}
		
public String getAlertText() {
	String text = "";
	try {
		Alert alert=driver.switchTo().alert();
		text=alert.getText();
		
		System.out.println("The alert "+text+" is displayed.");
	}  catch (NoAlertPresentException e) {
		System.out.println("There is no alert present.");
	} 
	catch (WebDriverException e) {
		System.out.println("WebDriverException : "+e.getMessage());
	}  
	return text;
	}
	
	public void typeAlert(String data) {
driver.switchTo().alert().sendKeys(data);
	}

	public void switchToWindow(int index) {
Set<String> er;
try {
	er = driver.getWindowHandles();
	List<String> eWindow=new ArrayList<>(er);
	driver.switchTo().window(eWindow.get(index));

} catch (Exception e) {
	// TODO Auto-generated catch block
	
}

	finally{
		takeSnap();
	}
	
	}

	@Override
	public void switchToWindow(String title) {
		
		Set<String> ewi;
		try {
			ewi = driver.getWindowHandles();
			for(String eachWindow : ewi) {
				driver.switchTo().window(eachWindow);
				if(driver.getTitle().equals(title)) {
					break;
				}
				 System.out.println("the window title is "+title+" is switched" );
			}}
				
		catch (NoSuchWindowException e) {
      
		}
		finally
		{
			  takeSnap();
		}
		
	
}

	

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
try {
	driver.switchTo().frame(index);
	System.out.println("move to frame");
	
} catch (Exception e) {
System.err.println("didnt move to frame");	
}
finally{
	takeSnap();
}



	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);

	}

	@Override
	public void switchToFrame(String idOrName) {
		driver.switchTo().frame(idOrName);
	}

	@Override
	public void defaultContent() {
		driver.switchTo().defaultContent();
	}

	@Override
	public boolean verifyUrl(String url) {
	
		if (driver.getCurrentUrl().equals(url)) {
			System.out.println("The url: "+url+" matched successfully");
		return true;
		} else {
			System.out.println("The url: "+url+" not matched");
		}
		return false;
	}



	
	public boolean verifyTitle(String title) {
		if (driver.getTitle().equals(title)) {
			System.out.println("Page title: "+title+" matched successfully");
		return true;
		} else {
			System.out.println("Page url: "+title+" not matched");
		}

		return false;
	}

	@Override
	public void takeSnap() {

		try {
			File source = driver.getScreenshotAs(OutputType.FILE);
			File dest = new File("./Snaps/snapshot"+i+".png");
			FileUtils.copyFile(source, dest);
		} catch (IOException e) {
			
			System.err.println("Snapshot"+i+"could not be captured");
		}
		i++;
		}
 


	@Override
	public void close() {
		driver.close();
	}
	public void quit() {
		driver.quit();
	
	}

}
