package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CreateLead extends Annotations {
	@BeforeTest(groups="smoke")   
	public void setData() {
		 testcaseName = "CreateLead";
		 testcaseDec = "Create a new Lead in leaftaps";
		 author = "buvana";
		 category = "Smoke";
	}
	
	@Test(groups="smoke",dataProvider="createData")
	public void loginCreate(String Cname,String Fname,String Lname) {
		
		// Refer annotations for login 
		WebElement eleCrmsfa = locateElement("link", "CRM/SFA");
		click(eleCrmsfa);
		
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompany, Cname);
		WebElement eleFName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFName, Fname);
		WebElement eleLName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLName, Lname);
		/*WebElement dd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(dd, "Conference");
		WebElement eleCLead = locateElement("class", "smallSubmit");
		click(eleCLead);
		WebElement eleFname = locateElement("xpath", "//span[@id='viewLead_firstName_sp']");
		String fname = getElementText(eleFname);
		WebElement eleLname = locateElement("xpath", "//span[@id='viewLead_lastName_sp']");
		String lname = getElementText(eleLname);
		if (fname.equalsIgnoreCase("Renuka")&& lname.equalsIgnoreCase("Sudharson")) {
			System.out.println("Lead created successfully");
		}*/
	}
	@DataProvider(name="createData")
		public Object[] fetchData()  {
		Object data[][]=new Object[2][3];
		data[0][0]="Leaf";
		data[0][1]="uva";
		data[0][2]="g";
		data[1][0]="Leaftest";
		data[1][1]="uvani";
		data[1][2]="N";
		return data;
		
		
		
		
		
	}

}





